// 为安卓低版本编写
// 测试手机型号  Honor CHM-TL00H
// 安卓版本 // 4.4.2

var t = 60 ;
task()


function task(){
    // 是否运行脚本,
    let isAMRunClock = false
    let isPMRunClock = false
    
    
    let id = setInterval(function(){

        let conf = getConfig()
        if(!conf.isClock){
            console.log("未开启执行...");
            return ;
        }
        
        let week = new Date().getDay();
        if(!conf.clockDate.includes(week)){
            console.log("不在打卡日期内");
            return ;
        }
        console.log("isAMRunClock-1 :%s", isAMRunClock );
        // 判断是否重置打开状态
        let state = resetState (conf, isAMRunClock, isPMRunClock)
        isAMRunClock = state.isAMRunClock
        isPMRunClock = state.isPMRunClock
        console.log("isAMRunClock-2 :%s", isAMRunClock );
        // 上午
        if(!isAMRunClock){
           if(calAmTime(conf.clockStartTime)){
                clockIn()
                isAMRunClock = true ;
                sendMail("(上午)")
           }
           console.log("isAMRunClock-3 :%s", isAMRunClock );
        }else{
            console.log("AM已执行...");
        }
        
        // 下午
        if(!isPMRunClock){
            if(calPmTime(conf.clockEndTime)){
                 clockIn()
                 isPMRunClock = true ;
                 sendMail("(下午)")
            }
         }else{
             console.log("PM已执行...");
         }
    }, t * 1000)
}



// 每天00 到1点之间重置打卡状态
function resetState (conf, isAMRunClock, isPMRunClock){

    if(conf.isReset){
        console.log("重置打卡状态...");
        isAMRunClock = false
        isPMRunClock = false
        // return ;
    }
    
    // 当前时间
    let nowHour =  new Date().getHours()
    let nowMinute =  new Date().getMinutes()
    let nowTime = nowHour+""+nowMinute
    console.log("判断是否在凌晨时间  +nowTime : %s", nowTime);
    if( +nowTime > 0 && +nowTime < 100){
        console.log("进入凌晨时间判断逻辑");
        isAMRunClock = false 
        isPMRunClock = false 
        // return ;
    }

    console.log("上午是否打卡:%s, 下午是否打卡: %s",isAMRunClock, isPMRunClock);

    return {
        "isAMRunClock":isAMRunClock,
        "isPMRunClock":isPMRunClock,
    }

}

// // 检测是否到达 上班打卡时间
function calAmTime (startTime){
    // 执行时间
    let hour =  startTime.split(":")[0]
    let minute =  startTime.split(":")[1]
    let runTime = hour+minute
    // 当前时间
    let nowHour =  new Date().getHours()
    let nowMinute =  new Date().getMinutes()
    if(nowMinute<10){
        nowMinute = "0" + nowMinute ;
    }
    let nowTime = nowHour+""+nowMinute
    log("AM : runTime: %s,  nowTime: %s", +runTime, +nowTime)   
    // 10:30之后不在执行
    if(+nowTime > +runTime && +nowTime < 1030){
        return true 
    }
}

// 检测是否到达 下班打卡时间
function calPmTime (endTime){
    // 执行时间
    let hour =  endTime.split(":")[0]
    let minute =  endTime.split(":")[1]
    let runTime = hour+minute
    // 当前时间
    let nowHour =  new Date().getHours()
    let nowMinute =  new Date().getMinutes()
    if(nowMinute<10){
        nowMinute = "0" + nowMinute ;
    }
    let nowTime = nowHour+""+nowMinute
    log("PM : runTime: %s,  nowTime: %s", +runTime, +nowTime)   
    // 20:30之后不在执行
    if(+nowTime > +runTime && +nowTime < 2000){
        return true 
    }
}


function getConfig(){
   var conf = {} ;
   try {
    let url = "https://so.ztengit.com/eduMap/api/pc/system/dict/data/dictType/dd"
    let res = http.get(url);
    // console.log("res:",res.body.string());
    res = JSON.parse(res.body.string().trim())  ;

    
    if(res.code == 200){
        res.data.forEach(e => {
            if(e.dictLabel == 'isClock'){
                conf.isClock = e.dictValue == 'true'
            }
            if(e.dictLabel == 'clockDate'){
                conf.clockDate = e.dictValue
            }
            if(e.dictLabel == 'clockStartTime'){
                conf.clockStartTime = e.dictValue
            }
            if(e.dictLabel == 'clockEndTime'){
                conf.clockEndTime = e.dictValue
            }
            if(e.dictLabel == 'isReset'){
                conf.isReset = e.dictValue == 'true'
            }
        });
    }
    console.log(conf);
   } catch (error) {
       
   }
    return conf ;
}

function sendMail(params){
    let url = "https://www.19911118.xyz/api/xcx/email/dingding?email=903900287@qq.com&title=钉钉打卡通知&content=打卡成功" + params;
    let res = http.get(url);
    console.log("调用发送邮件通知完成", res);
}




function clockIn(){
    let appName = "钉钉"
    toastLog("开始唤醒屏幕")
    
    device.wakeUpIfNeeded();
    
    sleep(10 * 1000);
    
    home();

    // 开始启动app
    console.log('启动app...');
    app.launchApp(appName)
    
    sleep(20 * 1000)
    home()
    toastLog("返回屏幕")

    // i = 20 
    // setTimeout(() => {
    //     app.launchApp("一键锁屏极速版")
    // }, i*1000);
    
    // setInterval(function(){
    //     i--
    //     if(i>0){
    //         toastLog("锁屏倒计时 " + i)
    //     }
    // },1000)

}
