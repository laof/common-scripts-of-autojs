// ==UserScript==
// @name         武汉理工 考试练习辅助，自动刷时间
// @namespace    http://tampermonkey.net/
// @version      2.0.0
// @description  用于 武汉理工大学继续教育学院 的 考试练习辅助，自动刷时间， 自动 和老师交互等公共
// @author       zhoudafu
// @match        http*://wljy.whut.edu.cn/web/exercise.htm*
// @match        http*://wljy.whut.edu.cn/web/showexercise.htm*
// @match        http*://wljy.whut.edu.cn/web/ucenterdetail.htm*
// @grant        unsafeWindow

// @updateURL https://gitee.com/zhuoyufei/common-scripts-of-autojs/raw/master/05/demo.js

// ==/UserScript==
// @require      file:///G:\work\project_2021\继续教育\油猴脚本\goeduscript\src\武汉理工.js
/** 
 */

(function () {
    'use strict';
// $(function () {

//============================================================

var storage=window.localStorage;
console.log("=====hello world====")
console.log("jquery version : ", 　$.fn.jquery)
$('#gerenzhongxin, .padd-body, #doLogin').prepend('<div id="yh_div">由于某些不可控原因</br>该脚本已经无限期暂停使用，</br>如果还想使用的，</br>后续将提供有偿服务，</br>更多详情请加qq号 <a rel="nofollow" target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=343286602&site=qq&menu=yes">343286602</a> 了解</div>');
$('#gerenzhongxin, .padd-body, #doLogin').prepend('<div id="yh_msg"></div>');

$('#yh_div').append(loading());



// $("#yh_btn0").hide()
loadGlobalCss(createCss())

const pathname = window.location.pathname
if("/web/ucenterdetail.htm" == pathname){
    $("#yh_btn1").hide();
    $("#yh_btn2").hide();
    $("#yh_btn3").hide();
    $("#yh_btn4").hide();
}else{
    $("#yh_btn5").hide();
    $("#yh_btn6").hide();
    $("#yh_btn7").hide();
    $("#yh_btn8").hide();
}


function loading(){
 return `    
 <!-- loading -->
 <div class="modal fade" id="loading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop='static'>
   <div class="modal-dialog" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <h4 class="modal-title" id="myModalLabel">提示</h4>
       </div>
       <div class="modal-body">
         请稍候。。。<span id="result"></span>
       </div>
     </div>
   </div>
 </div>
 ` 
}

var removeid = setInterval(function(){
    $(".enable").removeAttr("disabled")
}, 1000)



function sleep(delay) {
    for(var t = Date.now(); Date.now() - t <= delay;);
}



function isEmpty(val){
    if(val==undefined || val == null || val == ""){
        return true ;
    }
    return false ;
}


Array.prototype.remove = function (val) {
    let index = this.indexOf(val);
     if (index > -1) {
          this.splice(index, 1);
        }
 };


/**
 * 加载全局css 文件，并放到head中
 */
function loadGlobalCss(css){
    console.log("加载全局样式....");
    var style = document.createElement("style");
    style.type = "text/css";
    var text = document.createTextNode(css);
    style.appendChild(text);
    var head = document.getElementsByTagName("head")[0];
    head.appendChild(style);

}


/**
 * 这里写css 文件
 */
function createCss(){
var globalCss = heredoc(function(){/*
.exams-topnav {
    display:none;
}
#yh_div {
    background-color:#dbd990;
    top:100px;
    right:60px;
    position:fixed;
    z-index:100000;
    
}
#yh_btn1, #yh_btn2, #yh_btn3, #yh_btn4 {
    disabled:false;
    color:red; 
    font-size:15px;
}

.red{
    color:red; 
}

*/});
return globalCss ;
}




function getCookie(cookieName){  
    var cookieValue="";  
    if (document.cookie && document.cookie != '') {   
        var cookies = document.cookie.split(';'); 
        console.log("cookies:",cookies);
        for (var i = 0; i < cookies.length; i++) {   
             var cookie = cookies[i].trim();
             if (cookie.substring(0, cookieName.length + 1).trim() == cookieName.trim() + "=") {  
                   cookieValue = cookie.substring(cookieName.length + 1, cookie.length);
                   break;  
             }  
         }  
    }   
    return cookieValue;  
} 


// 生成随机数
function random(min,max){
    let n = Math.random()*(max-min+1)+min;
    if(n>30){
        return 30 ;
    }
    return n;
}


 //获取url中的参数
 function queryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return unescape(r[2]); return null; //返回参数值
}


String.prototype.startWith=function(str){     
    var reg=new RegExp("^"+str);     
    return reg.test(this);        
  } 
   
String.prototype.endWith=function(str){     
var reg=new RegExp(str+"$");     
return reg.test(this);        
}


String.prototype.format = function(args) {
    var result = this;
    if (arguments.length > 0) {    
        if (arguments.length == 1 && typeof (args) == "object") {
            for (var key in args) {
                if(args[key]!=undefined){
                    var reg = new RegExp("({" + key + "})", "g");
                    result = result.replace(reg, args[key]);
                }
            }
        }
        else {
            for (var i = 0; i < arguments.length; i++) {
                if (arguments[i] != undefined) {
                    //var reg = new RegExp("({[" + i + "]})", "g");//这个在索引大于9时会有问题
                    var reg = new RegExp("({)" + i + "(})", "g");
                    result = result.replace(reg, arguments[i]);
             }
          }
       }
   }
   return result;
}


 function heredoc(fn) {
    return fn.toString().split('\n').slice(1,-1).join('\n') + '\n'
}


console.log(createCss());


// =================================================================
// 这里是结束符
// });
})();

