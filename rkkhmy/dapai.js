// ==UserScript==
// @name         西兵互娱
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  西兵互娱
// @author       flg
// @match        http://xxb.rkkhmy.work/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        unsafeWindow

// @require      file:///G:\work\project_2021\autojs\1\common-scripts-of-autojs\rkkhmy\dapai.js

// ==/UserScript==

(function () {
    'use strict';
console.log("西兵互娱... ing");

var wsk ;
//WebSocket
var connectSocket = function connectSocket(url, openCallback, messageCallback, closeCallback, errorCallback) {
    wsk = new WebSocket(url);
    wsk.onopen = openCallback;
    wsk.onmessage = messageCallback;
    wsk.onclose = closeCallback;
    wsk.onerror = errorCallback;
}

console.log("appData : ", appData);
console.log("globalData : ", globalData);

function logMessage(message) {
    console.log("=========>",message);
};

var reconnectSocket = function reconnectSocket() {
    console.log("==>websocket 连接 ing");
    if (!appData.isReconnect) {
        return;
    }
    if (globalData.roomStatus == 4) {
        return;
    }
    if (wsk) {
        if (ws.readyState == 1) { //websocket已经连接
            return;
        }
        wsk = null;
    }
    connectSocket(globalData.socket, wsOpenCallback, wsMessageCallback, wsCloseCallback, wsErrorCallback);
}

var wsOpenCallback = function wsOpenCallback(data) {
    logMessage('==>websocket is opened');
    appData.connectOrNot = true;

    if (appData.heartbeat) {
        clearInterval(appData.heartbeat);
    }

    appData.heartbeat = setInterval(function() {
        appData.socketStatus = appData.socketStatus + 1;

        if (appData.socketStatus > 1) {
            appData.connectOrNot = false;
        }

        if (appData.socketStatus > 3) {
            if (appData.isReconnect) {
                window.location.href = window.location.href + "&id=" + 10000 * Math.random();
            }
        }

        if (wsk.readyState == WebSocket.OPEN) {
            wsk.send('@');
        }
    }, 3000);

    socketModule.sendPrepareJoinRoom();
}

let peibaoif= 0 ;
var wsMessageCallback = function wsMessageCallback(evt) {
    appData.connectOrNot = true;
    console.log("===> 培宝是个傻子 ",  peibaoif++);
    if (evt.data == '@') {
        appData.socketStatus = 0;
        return 0;
    }

    var obj = eval('(' + decodeMsg(evt.data) + ')');
    //var obj = eval('(' + evt.data + ')');
    logMessage(obj);

    if (obj.result == -201) {
        viewMethods.clickShowAlert(31, obj.result_message);
    } else if (obj.result == -202) {
        appData.isReconnect = false;
        socketModule.closeSocket();
        viewMethods.clickShowAlert(32, obj.result_message);
    } else if (obj.result == -203) {
        viewMethods.reloadView();
    }

    if (obj.result != 0) {
        if (obj.operation == wsOperation.JoinRoom) {
            if (obj.result == 1) {
                if (obj.data.alert_type == 1) {
                    viewMethods.clickShowAlert(1, obj.result_message);
                } else if (obj.data.alert_type == 2) {
                    viewMethods.clickShowAlert(2, obj.result_message);
                } else if (obj.data.alert_type == 3) {
                    viewMethods.clickShowAlert(11, obj.result_message);
                } else {
                    viewMethods.clickShowAlert(7, obj.result_message);
                }
            } else if (obj.result == -1) {
                viewMethods.clickShowAlert(7, obj.result_message);
            } else {
                viewMethods.clickShowAlert(7, obj.result_message);
            }
        } else if (obj.operation == wsOperation.ReadyStart) {
            if (obj.result == 1) {
                viewMethods.clickShowAlert(1, obj.result_message);
            }
        } else if (obj.operation == wsOperation.PrepareJoinRoom) {

            if (obj.result > 0) {
                socketModule.processGameRule(obj);
            }

            if (obj.result == 1) {
                if (obj.data.alert_type == 1) {
                    viewMethods.clickShowAlert(1, obj.result_message);
                } else if (obj.data.alert_type == 2) {
                    viewMethods.clickShowAlert(2, obj.result_message);
                } else if (obj.data.alert_type == 3) {
                    viewMethods.clickShowAlert(11, obj.result_message);
                } else {
                    viewMethods.clickShowAlert(7, obj.result_message);
                }
            } else if (obj.result == -1) {
                viewMethods.clickShowAlert(7, obj.result_message);
            } else {
                viewMethods.clickShowAlert(7, obj.result_message);
            }
        } else if (obj.operation == wsOperation.RefreshRoom) {
            window.location.href = window.location.href + "&id=" + 10000 * Math.random();
        }

        appData.player[0].is_operation = false;
    } else {
        if (obj.operation == wsOperation.PrepareJoinRoom) {
            socketModule.processPrepareJoinRoom(obj);
        } else if (obj.operation == wsOperation.JoinRoom) {
            socketModule.processJoinRoom(obj);
        } else if (obj.operation == wsOperation.RefreshRoom) {
            socketModule.processRefreshRoom(obj);
        } else if (obj.operation == wsOperation.AllGamerInfo) {
            socketModule.processAllGamerInfo(obj);
        } else if (obj.operation == wsOperation.UpdateGamerInfo) {
            socketModule.processUpdateGamerInfo(obj);
        } else if (obj.operation == wsOperation.UpdateAccountStatus) {
            socketModule.processUpdateAccountStatus(obj);
        } else if (obj.operation == wsOperation.UpdateAccountShow) {
            socketModule.processUpdateAccountShow(obj);
        } else if (obj.operation == wsOperation.UpdateAccountMultiples) {
            socketModule.processUpdateAccountMultiples(obj);
        } else if (obj.operation == wsOperation.StartLimitTime) {
            socketModule.processStartLimitTime(obj);
        } else if (obj.operation == wsOperation.CancelStartLimitTime) {
            socketModule.processCancelStartLimitTime(obj);
        } else if (obj.operation == wsOperation.GameStart) {
            socketModule.processGameStart(obj);
        } else if (obj.operation == wsOperation.UpdateAccountScore) {
            socketModule.processUpdateAccountScore(obj);
        } else if (obj.operation == wsOperation.Win) {
            socketModule.processWin(obj);
        } else if (obj.operation == wsOperation.BroadcastVoice) {
            socketModule.processBroadcastVoice(obj);
        } else if (obj.operation == wsOperation.StartBet) {
            socketModule.processStartBet(obj);
        } else if (obj.operation == wsOperation.StartShow) {
            socketModule.processStartShow(obj);
        } else if (obj.operation == wsOperation.MyCards) {
            socketModule.processMyCards(obj);
        } else if (obj.operation == wsOperation.BreakRoom) {
            socketModule.processBreakRoom(obj);
        } else if (obj.operation == 'Oc') {
            let data = obj.data;
            for (let i = 0; i < appData.player.length; i++) {
                for (let j = 0; j < data.length; j++) {
                    if (appData.player[i].account_id == data[j].player_id) {
                        let cardType = data[j].card_type;
                        let kind = 0;
                        if (cardType == 1) {
                            kind = 0;
                        } else if (cardType == 4) {
                            kind = 10;
                        } else if (cardType == 5) {
                            kind = 11;
                        } else if (cardType == 6) {
                            kind = 12;
                        } else if (cardType == 7) {
                            kind = 13;
                        } else if (cardType == 8) {
                            kind = 14;
                        } else if (cardType == 9) {
                            kind = 15;
                        } else if (cardType == 10) {
                            kind = 16;
                        } else if (cardType == 11) {
                            kind = 17;
                        } else if (cardType == 12) {
                            kind = 18;
                        } else if (cardType == 13) {
                            kind = 19;
                        } else if (cardType == 14) {
                            kind = 20;
                        } else {
                            kind = data[j].combo_point;
                        }
                        appData.player[i].kind = kind;
                        break;
                    }
                }
            }
        }

        //观战功能
        else if (obj.operation == wsOperation.GuestRoom) {
            socketModule.processGuestRoom(obj);
        }  else if (obj.operation == wsOperation.AllGuestInfo) {
            socketModule.processAllGuestInfo(obj);
        }  else if (obj.operation == wsOperation.UpdateGuestInfo) {
            socketModule.processUpdateGuestInfo(obj);
        } else {
            logMessage(obj.operation);
        }
    }
}


var wsCloseCallback = function wsCloseCallback(data) {
    console.log("===>websocket closed：");
    appData.connectOrNot = false;
    reconnectSocket();
}

var wsErrorCallback = function wsErrorCallback(data) {
    console.log("===>websocket onerror：");
}


reconnectSocket()



})();



