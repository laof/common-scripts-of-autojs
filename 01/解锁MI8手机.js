
toast('开始解锁MI8手机...');

var pwd = "8855"

function unlock(){
    let isLock = 是否锁屏();
    if(isLock){
        // 唤醒屏幕
        let flag = device.wakeUp();
        log("flag %s", flag)
        // 这里也可以直接使用,该函数表示，如果屏幕未唤醒则唤醒
        // device.wakeUpIfNeeded();
    }else{
        toastLog("已经解锁")
    }
    let isSecure = 是否包含锁屏密码();
    log("是否包含锁屏密码 : %s", isSecure)
    if (isLock && isSecure) {
        sleep(200)
        小米锁屏上滑动作();
        输入密码动作(pwd);
    }
    toastLog("解锁完成!")
}


function 输入密码动作(password) {
    // 由于可能存在的误操作，解锁前先删除密码盘中的数字
    longClick("删除",0)
    sleep(200)
    // 开始输入密码
    for (var i = 0; i < password.length; i++) {
        a = password.charAt(i)
        sleep(500)
        b = text(a).findOne().bounds()
        click(b.centerX(), b.centerY())
    }
}

function 小米锁屏上滑动作() {
    var xyArr = [220]
    var x0 = device.width / 2
    var y0 = device.height / 4 * 3 -430
    var angle = 0
    var x = 0
    var y = 0
    for (let i = 0; i < 50; i++) {
        y = x * tan(angle)
        // log(y)
        if ((y0 - y) < 0) {
            break
        }
        var xy = [x0 + x, y0 - y]
        xyArr.push(xy)
        x += 5;
        angle += 3
    }
    // log(xyArr)
    gesture.apply(null, xyArr)
    function tan(angle) {
        return Math.tan(angle * Math.PI / 180);
    }
}

function 是否锁屏(){
    importClass(android.app.KeyguardManager)
    importClass(android.content.Context)
    var km = context.getSystemService(Context.KEYGUARD_SERVICE);
    // ture-锁屏， false-已解锁
    return km.isKeyguardLocked();
}

function 是否包含锁屏密码(){
    importClass(android.app.KeyguardManager)
    importClass(android.content.Context)
    var km = context.getSystemService(Context.KEYGUARD_SERVICE);
    //  true:有锁屏密码（包含SIM卡锁） false:未设置
    return km.isKeyguardSecure();
}

module.exports.unlock = unlock ;
module.exports.小米锁屏上滑动作 = 小米锁屏上滑动作 ;
