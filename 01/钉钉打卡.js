
var unlockmi8 = require("./解锁MI8手机.js")

auto.waitFor();
let appName="钉钉"
let stopName="结束运行"
// let orgName = "浙江智腾信息科技有限公司" ;
let orgName = "浙江智腾信息科技有限公司杭州研发中心" ;

// 解锁手机
unlockmi8.unlock()

// clearApp()

// 先关闭钉钉，防止钉钉已经打开出现其他异常情况
closeApp(appName, stopName);
// 开始启动app
console.log('启动app...');
app.launchApp(appName)
sleep(2000)

while(!click("搜索"));
sleep(1000)

// 方案一搜索
// id('search_src_text').text('搜索').setText("考勤打卡")
// sleep(1000)
// while(!id("item_name_tv").text("考勤打卡").findOne().parent().click());

// 方案二 点击历史记录
while(!id("display_name").className("android.widget.TextView").text("考勤打卡").findOne().parent().click());

sleep(1000)

while(!click(orgName));
sleep(3000)

if(text("上班打卡").exists()){
    while(!click("上班打卡"));
}
if(text("下班打卡").exists()){
    while(!click("下班打卡"));
} 

sleep(3000)
if(text("继续打卡").exists()){
    click("继续打卡")
}

// 打卡完成关闭app
console.log('打卡完成关闭app，并回到主页');
closeApp(appName, stopName);
sleep(100)

// 回到首页
home()
exit()

function clearApp(){
    recents();
    sleep(1000)
    id("clearAnimView").findOne().click()
    home()
    console.log("清理后台任务完成");
}


function closeApp(appName, stopName){
    // 先打开钉钉，保证结束运行按钮可用
    app.launchApp(appName)
    sleep(2000)
    console.log("begin close app %s ...", appName);
    app.openAppSetting(app.getPackageName(appName))

    let btn =  text(stopName)
    console.log("en:" + btn.enabled());
    if(!btn.enabled()){
        console.log("应用已关闭，%s按钮不可点击！", stopName);
        return 
    }
    console.log("点击结束运行按钮 ...");
    while(!click(stopName));
    sleep(2*1000);
    while(!click("确定"));
    sleep(2*1000);
    console.log("close app %s successful !", appName);
}