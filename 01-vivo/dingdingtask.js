

var clock = require("./钉钉打卡.js")
// 循环时间
let loopTime = 3 ;

task()

function task(){
    // 是否运行脚本,
    let isAMRunClock = false
    let isPMRunClock = false
    
    let id = setInterval(function(){

        let conf = getConfig()
        if(!conf.isClock){
            console.log("未开启执行...");
            return ;
        }
        let week = new Date().getDay();
        if(!conf.clockDate.includes(week)){
            console.log("不在打卡日期内");
            return ;
        }

        // 判断是否重置打开状态
        let state = resetState (conf, isAMRunClock, isPMRunClock)
        isAMRunClock = state.isAMRunClock
        isPMRunClock = state.isPMRunClock

        // 上午
        if(!isAMRunClock){
           if(calAmTime(conf.clockStartTime)){
                clock.clockIn()
                isAMRunClock = true ;
           }
        }else{
            console.log("AM已执行...");
        }
        
        // 下午
        if(!isPMRunClock){
            if(calPmTime(conf.clockEndTime)){
                clock.clockIn()
                 isPMRunClock = true ;
            }
         }else{
             console.log("AM已执行...");
         }
    }, loopTime * 1000)
}


// 每天00 到1点之间重置打卡状态
function resetState (conf, isAMRunClock, isPMRunClock){

    if(conf.isReset){
        console.log("重置打卡状态...");
        isAMRunClock = false
        isPMRunClock = false
        // return ;
    }
    
    // 当前时间
    let nowHour =  new Date().getHours()
    let nowMinute =  new Date().getMinutes()
    let nowTime = nowHour+""+nowMinute
    if( +nowTime > 0 && +nowTime < 100){
        isAMRunClock = false 
        isPMRunClock = false 
        // return ;
    }

    console.log(isAMRunClock, isPMRunClock);

    return {
        "isAMRunClock":isAMRunClock,
        "isPMRunClock":isPMRunClock,
    }

}


// // 检测是否到达 上班打卡时间
function calAmTime (startTime){
    // 执行时间
    let hour =  startTime.split(":")[0]
    let minute =  startTime.split(":")[1]
    let runTime = hour+minute
    // 当前时间
    let nowHour =  new Date().getHours()
    let nowMinute =  new Date().getMinutes()
    if(nowMinute<10){
        nowMinute = "0" + nowMinute ;
    }
    let nowTime = nowHour+""+nowMinute
    
    log("AM : runTime: %s,  nowTime: %s", +runTime, +nowTime)   
    // 10:30之后不在执行
    if(+nowTime > +runTime && +nowTime < 1030){
        return true 
    }
}

// 检测是否到达 下班打卡时间
function calPmTime (endTime){
    // 执行时间
    let hour =  endTime.split(":")[0]
    let minute =  endTime.split(":")[1]
    let runTime = hour+minute
    // 当前时间
    let nowHour =  new Date().getHours()
    let nowMinute =  new Date().getMinutes()
    if(nowMinute<10){
        nowMinute = "0" + nowMinute ;
    }
    let nowTime = nowHour+""+nowMinute
    log("PM : runTime: %s,  nowTime: %s", +runTime, +nowTime)
    // 20:30之后不在执行
    if(+nowTime > +runTime && +nowTime < 2000){
        return true 
    }
}

function getConfig(){
    let url = "http://1.15.21.98:8081/api/pc/system/dict/data/dictType/dd"
    let res = http.get(url);
    // console.log("res:",res.body.string());
    res = JSON.parse(res.body.string().trim())  ;

    var conf = {} ;
    
    if(res.code == 200){
        res.data.forEach(e => {
            if(e.dictLabel == 'isClock_xl'){
                conf.isClock = e.dictValue == 'true'
            }
            if(e.dictLabel == 'clockDate_xl'){
                conf.clockDate = e.dictValue
            }
            if(e.dictLabel == 'clockStartTime_xl'){
                conf.clockStartTime = e.dictValue
            }
            if(e.dictLabel == 'clockEndTime_xl'){
                conf.clockEndTime = e.dictValue
            }
            if(e.dictLabel == 'isReset_xl'){
                conf.isReset = e.dictValue == 'true'
            }
        });
    }
    console.log(conf);
    return conf ;
}
